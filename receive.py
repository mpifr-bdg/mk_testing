import os
import json
import asyncio
import time
import argparse
from argparse import RawTextHelpFormatter

import mpikat.utils.dada_tools as dada

from mk_testing.mkrecv import Mkreceiver

async def main_wrapper(parser: argparse.ArgumentParser):

    nic_ip = parser.parse_args().nic_ip
    mc_ip = parser.parse_args().mc_ip
    mc_port = int(parser.parse_args().mc_port)
    ibv = bool(int(parser.parse_args().ibv))
    duration = int(parser.parse_args().duration)
    ofile = parser.parse_args().ofile
    with open(parser.parse_args().config_file, "r") as f:
        config = json.load(f)


    buffer = dada.DadaBuffer(config["buffer_size"])
    await buffer.create()
    if ofile:
        buffer_reader = dada.DbDisk(buffer.key, ofile)
    else:
        buffer_reader = dada.DbNull(buffer.key)
    buffer_reader.start()
    buffer_writer = Mkreceiver(buffer.key, nic_ip, mc_ip, mc_port, config, ibv=ibv)
    buffer_writer.start()

    # Run the reception for at least the given duration
    start = time.time()
    while True:
        if time.time()-start > duration:
            print(time.time()-start, duration)
            break
        time.sleep(.5)

    # Clean up
    buffer_writer.stop()
    buffer_reader.stop()
    await buffer.destroy()


if __name__ == "__main__":
    abspath = os.path.dirname(os.path.abspath(__file__))
    parser = argparse.ArgumentParser(formatter_class=RawTextHelpFormatter)
    parser.add_argument('--nic_ip','-ip', action='store', default="10.10.1.81", dest="nic_ip",
                        help="The network interface ip address")
    parser.add_argument('--mc_ip','-mc', action='store', default="225.0.0.112+3", dest="mc_ip",
                        help="The multicast ip address")
    parser.add_argument('--mc_port','-p', action='store', default=7155, dest="mc_port",
                        help="The multicast port")
    parser.add_argument('--duration', '-t', action='store', default=30, dest="duration",
                        help="Duration of the transmission")
    parser.add_argument('--ibv', '-v', action='store', default=True, dest="ibv",
                        help="Use ib verbs for network capturing")
    parser.add_argument('--output_file', '-o', action='store', default="", dest="ofile",
                        help="The output file to store the received data")
    parser.add_argument('--config_file', '-c', action='store',
                        default= os.path.join(abspath, "conf/fbfuse_mode1.json"), dest="config_file",
                        help="The config file containing the stream description")

    asyncio.run(main_wrapper(parser))