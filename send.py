#!/usr/bin/python
import os
import time
import json
import asyncio
import argparse

import mpikat.utils.dada_tools as dada
from mk_testing.mksend import MksendPool
from mk_testing.ip_util import get_interface_numa_node

async def main_wrapper(parser: argparse.ArgumentParser):

    interface = parser.parse_args().interface
    mc_ip = parser.parse_args().mc_ip
    mc_port = int(parser.parse_args().mc_port)
    ibv = bool(int(parser.parse_args().ibv))
    duration = int(parser.parse_args().duration)
    ifile = str(parser.parse_args().input_file)
    nsender = int(parser.parse_args().nsender)
    nreader = int(parser.parse_args().nreader)
    multi_ip = bool(parser.parse_args().multi_ip)
    with open(parser.parse_args().config_file, "r") as f:
        config = json.load(f)
    numa_node = get_interface_numa_node(interface)
    nbuffer = nsender//nreader
    # Allocate buffers
    sub_buffer = []
    buffer_creation_tasks = []
    buffer_destroy_tasks = []
    main_buffer = dada.DadaBuffer(config["buffer_size"]*nbuffer, node_id=numa_node)
    buffer_creation_tasks.append(asyncio.create_task(main_buffer.create()))

    for i in range(nbuffer):
        if i+1 == nbuffer and nsender%nreader:
            sub_buffer.append(dada.DadaBuffer(config["buffer_size"], n_reader=nsender%nreader, node_id=numa_node))
        else:
            sub_buffer.append(dada.DadaBuffer(config["buffer_size"], n_reader=nreader, node_id=numa_node))

        buffer_creation_tasks.append(asyncio.create_task(sub_buffer[-1].create()))
    await asyncio.gather(*buffer_creation_tasks)

    # Setup the transmission environment
    pool = MksendPool(interface, config)
    pool.setup(sub_buffer, mc_ip, mc_port, nreader, multi_ip, ibv)
    pool.start()

    # Create splitter
    splitter = dada.DbSplitDb(main_buffer.key, [buf.key for buf in sub_buffer])
    splitter.start()

    # Create the writer
    if os.path.exists(ifile):
        n_reads = duration / (os.path.getsize(ifile) / (config["data_rate"] // nsender))
        buffer_writer = dada.DiskDb(main_buffer.key, ifile, n_reads=n_reads, data_rate=config["data_rate"])
    else:
        buffer_writer = dada.JunkDb(main_buffer.key, data_rate=int(config["data_rate"]), duration=duration)
    buffer_writer.start()


    # Run the setup
    start = time.time()
    try:
        while time.time()-start < duration:
            time.sleep(.5)
        time.sleep(1)
    except KeyboardInterrupt:
        print("KeyboardInterrupt detected. Exiting...")
    # Clean up processes
    buffer_writer.stop()
    splitter.stop()
    pool.clean()

    # Deallocate memory
    buffer_destroy_tasks.append(asyncio.create_task(main_buffer.destroy()))
    for buf in sub_buffer:
        buffer_destroy_tasks.append(asyncio.create_task(buf.destroy()))
    await asyncio.gather(*buffer_destroy_tasks)


if __name__ == "__main__":
    abspath = os.path.dirname(os.path.abspath(__file__))
    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--interface','-i', action='store', default="ens1", dest="interface",
                        help="The multicast ip address")
    parser.add_argument('--mc_ip','-mc', action='store', default="225.0.0.112+3", dest="mc_ip",
                        help="The multicast ip address")
    parser.add_argument('--mc_port','-p', action='store', default=7155, dest="mc_port",
                        help="The multicast port")
    parser.add_argument('--duration', '-t', action='store', default=60, dest="duration",
                        help="The number of packets to send")
    parser.add_argument('--ibv', '-v', action='store', default=True, dest="ibv",
                        help="Use ib verbs for network capturing")
    parser.add_argument('--input_file', '-f', action='store', default="", dest="input_file",
                        help="An input file to read from. If not set or exists, create random payload")
    parser.add_argument('--n_sender', '-n', action='store', default=1, dest="nsender",
                        help="Number of sending instances. The network interface addresses are derived from the number of sender and the base ip")
    parser.add_argument('--n_reader', '-r', action='store', default=1, dest="nreader",
                        help="Number of reading clients per buffer")
    parser.add_argument('--multi_ip', '-s', action='store', default=0, dest="multi_ip",
                        help="If set, all sending processes use a different IP, otherwise each sender gets the same IP."+
                        "In case the asssociated interface has not enough virtual IPs, IPs are added and removed after the sending is completed")
    parser.add_argument('--config_file', '-c', action='store',
                        default= os.path.join(abspath, "conf/fbfuse_mode1.json"), dest="config_file",
                        help="The config file containing the stream description")

    asyncio.run(main_wrapper(parser))