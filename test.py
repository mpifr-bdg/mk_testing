import unittest
import os
import glob
import time
import shutil
import numpy as np

import mpikat.utils.dada_tools as dada
from mk_testing.tv_generator import TVGenerator
from mk_testing.mkrecv import Mkreceiver
from mk_testing.mksend import Mksend

TEST_CONF = {
    "sample_clock":1712000000,
    "data_rate": 342400000,
    "item_2_list":"0:64:1",
    "item_3_list":"0:64:16",
    "heap_size":16384,
    "timestamp_step":2097152,
    "timestamp_modulus":2097152,
    "buffer_size":268435456
}

base_dir = os.path.dirname(os.path.abspath(__file__))

class Test_mk_send_recv(unittest.IsolatedAsyncioTestCase):


    def setUp(self) -> None:
        # Create a testvector
        self.gen = TVGenerator(TEST_CONF)
        self.gen.generate()
        self.odir = os.path.join(base_dir, "output")
        os.mkdir(self.odir)

    async def asyncSetUp(self) -> None:
        # Setup receiver
        self.mkrecv_buffer = dada.DadaBuffer(TEST_CONF["buffer_size"])
        await self.mkrecv_buffer.create()
        self.disk_writer = dada.DbDisk(self.mkrecv_buffer.key, self.odir)
        self.disk_writer.start()
        self.mkrecv_proc = Mkreceiver(self.mkrecv_buffer.key, "127.0.0.1", "225.0.0.1", 7155, TEST_CONF, False)
        self.mkrecv_proc.start()

        # Setup Sending
        self.mksend_buffer = dada.DadaBuffer(TEST_CONF["buffer_size"])
        await self.mksend_buffer.create()
        self.disk_reader = dada.DiskDb(self.mksend_buffer.key, self.gen.ofile, n_reads=16, data_rate=TEST_CONF["data_rate"])
        self.mksend_proc = Mksend(self.mksend_buffer.key, "127.0.0.1", "225.0.0.1", 7155, TEST_CONF, False)
        self.mksend_proc.start()


    def test_transmission(self):

        proc = self.disk_reader.start()
        while proc.is_alive():
            time.sleep(0.5)
        testvector = np.fromfile(self.gen.ofile, dtype=np.int8)
        files = sorted(glob.glob(self.odir + "/*"))
        for file in files[:-1]:
            output = np.fromfile(file, dtype=np.int8, offset=4096).reshape(-1, TEST_CONF["buffer_size"])
            for batch in output:
                self.assertTrue(np.all(batch == testvector))

    async def asyncTearDown(self):
        # Clean up receiver
        self.mkrecv_proc.stop()
        self.disk_writer.stop()
        await self.mkrecv_buffer.destroy()
        # Clean uo sending
        self.disk_reader.stop()
        self.mksend_proc.stop()
        await self.mksend_buffer.destroy()

    def tearDown(self) -> None:
        # Remove the testvector
        os.remove(self.gen.ofile)
        shutil.rmtree(self.odir)

if __name__ == '__main__':
    unittest.main()
