import numpy as np


class TVGenerator:

    def __init__(self, conf: dict, mode="noise", ofile="testvector.dat"):
        self.conf = conf
        self.mode = mode
        self.ofile = ofile

    def generate(self):
        out = getattr(self, self.mode)()
        out.tofile(self.ofile)

    def noise(self) -> np.ndarray:
        return (np.random.rand(self.conf["buffer_size"]) * 127).astype(np.int8)