import subprocess
import re

from mpikat.utils.ip_utils import ipstring_to_list
from mpikat.utils.ip_manager import IPManager


def get_interface_subnet(interface):
    try:
        output = subprocess.check_output(["ip", "addr", "show", "dev", interface]).decode("utf-8")
        subnet_match = re.search(r"inet\s(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/\d{1,2})", output)

        if subnet_match:
            subnet = subnet_match.group(1)
            return subnet
        else:
            return None
    except subprocess.CalledProcessError as e:
        print(f"Failed to get subnet for interface {interface}. Error: {e}")


def get_interface_ips(interface):
    command = ["ip", "addr", "show", "dev", interface]
    output = subprocess.check_output(command).decode("utf-8")

    ips = []
    lines = output.split("\n")
    for line in lines:
        if "inet " in line:
            ip = line.split()[1]
            ips.append(ip)

    return sorted(ips)

def get_interface_numa_node(interface):
    try:
        command = ["cat", f"/sys/class/net/{interface}/device/numa_node"]
        return subprocess.check_output(command).decode("utf-8").replace("\n","")
    except subprocess.CalledProcessError as e:
        print(f"Failed to get NUMA node for interface {interface}. Error: {e}")

def add_interface_ip(interface, ip_address):
    try:
        subprocess.check_output(["ip", "addr", "add", "dev", interface, ip_address])
        print(f"Successfully added IP address {ip_address} to interface {interface}")
    except subprocess.CalledProcessError as e:
        print(f"Failed to add IP address {ip_address} to interface {interface}. Error: {e}")

def remove_interface_ip(interface, ip_address):
    try:
        subprocess.check_output(["ip", "addr", "del", "dev", interface, ip_address])
        print(f"Successfully removed IP address {ip_address} from interface {interface}")
    except subprocess.CalledProcessError as e:
        print(f"Failed to remove IP address {ip_address} from interface {interface}. Error: {e}")

class IPStorage():

    def __init__(self, interface:str):
        self.interface = interface
        self.subnet = get_interface_subnet(self.interface)
        self.manager = IPManager(self.subnet)
        self.added_ips = []
        self.ip_list = []
        for ip in get_interface_ips(interface):
            self.ip_list.append(ip.split("/")[0])
        self.base_ip = self.ip_list[0].rsplit(".",1)[0]

    def get_ip_list(self, n_ips: int) -> list:
        if len(self.ip_list) < n_ips:
            ip_alloc_list = []
            for i in range(1, n_ips-len(self.ip_list)+1):
                ip_alloc_list.append(f"{self.base_ip}.{i}")
            self.added_ips = [item for item in ip_alloc_list if item not in self.ip_list]
            for ip in self.added_ips:
                add_interface_ip(self.interface, ip)
                self.ip_list.append(ip)
        return self.ip_list

    def remove_added_ips(self):
        for ip in self.added_ips:
            remove_interface_ip(self.interface, ip)
        self.added_ips = []

