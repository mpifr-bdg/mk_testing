import re
from threading import Thread
from mpikat.utils.dada_tools.reader import DadaClient
from mpikat.utils.core_manager import CoreManager

from mk_testing.ip_util import IPStorage, get_interface_numa_node
from mk_testing.formats import FBFUSE_INPUT

def extract_numbers(line: str):
    pattern = r"\d+"  # Match one or more digits
    matches = re.findall(pattern, line)
    numbers = [int(match) for match in matches]
    return numbers

class MksendMonitor():

    def __init__(self):
        self.total_heaps = 0
        self.heaps = []
        self.mcast = 0

    def stdout_handle(self, line):
        if "sending heap" not in line:
            return
        numbers = extract_numbers(line)
        self.total_heaps += 1
        while numbers[1] >= self.mcast:
            self.heaps.append(0)
            self.mcast += 1
        self.heaps[numbers[1]] += 1


class Mksend(DadaClient):
    def __init__(self, key, nic_ip, mc_ip, port, header, ibv=True, physcpu=None):
        self.monitor = MksendMonitor()
        super().__init__(key, stdout_handler=self.monitor.stdout_handle)
        self.nic_ip = nic_ip
        self.mc_ip = mc_ip
        self.port = port
        self.key = key
        self.ibv = ibv
        self.header = header

        if physcpu is not None:
            self.cmd = f"numactl --cpubind 0 --membind 0 mksend "
        else:
            self.cmd = "mksend "
        self.cmd += f" --header {self.header}"
        self.cmd += f" --dada-key {self.key}"
        self.cmd += f" --port {self.port}"
        if self.ibv:
            self.cmd += f" --ibv-if {self.nic_ip}"
        else:
            self.cmd += f" --udp-if {self.nic_ip}"
        self.cmd += f" {self.mc_ip}"

class MksendPool():

    def __init__(self, interface: str, desc: dict):
        self.ip_storage = IPStorage(interface)
        self.core_manager = CoreManager(get_interface_numa_node(interface))
        self.mksend_procs = []
        self.tasks = []
        self._running = False
        self.desc = desc

    def setup(self, dada_buffers:list, mc_ip:str, port:int, nreader_per_buffer:int, multi_ip:bool=False, ibv:bool=False) -> None:
        nbuffer = len(dada_buffers)
        nsender = nbuffer * nreader_per_buffer
        if multi_ip:
            ip_list = self.ip_storage.get_ip_list(nsender)
        else:
            ip_list = self.ip_storage.get_ip_list(1)
        self.desc_formatter = FBFUSE_INPUT(self.desc, nsender) # ToDo: make factory
        print("THE IP LIST IS: ", ip_list)
        for idx in range(nsender):
            self.tasks.append(self.core_manager.add_task(f"sender_{idx}", self.desc["cores"]))

        for idx, buf in enumerate(dada_buffers):
            for iidx in range(nreader_per_buffer):
                send_id = idx * nreader_per_buffer + iidx
                header = self.desc_formatter.get_header(send_id)
                if send_id >= nsender:
                    return
                if multi_ip:
                    self.add(Mksend(buf.key, ip_list[send_id], mc_ip, port, header, ibv,
                        self.core_manager.get_coresstr(f"sender_{send_id}")))
                else:
                    self.add(Mksend(buf.key, ip_list[0], mc_ip, port, header, ibv,
                        self.core_manager.get_coresstr(f"sender_{send_id}")))
    def clean(self):
        if self._running:
            self.stop()
        self.mksend_procs = []
        self.ip_storage.remove_added_ips()

    def add(self, sender: Mksend) -> None:
        self.mksend_procs.append(sender)

    def remove(self, attr:str, value:any):
        for proc in self.mksend_procs:
            val = getattr(proc, attr)
            if val == value:
                if self._running:
                    proc.stop()
                self.mksend_procs.remove(proc)

    def start(self):
        if not self._running:
            for proc in self.mksend_procs:
                proc.start()
            self._running = True

    def stop(self):
        total_heaps = 0
        if self._running:
            stop_tasks = []
            print("Stopping MksendPool")
            for i, proc in enumerate(self.mksend_procs):
                stop_task = Thread(target=lambda: proc.stop())
                stop_task.start()
                stop_tasks.append(stop_task)

            for i, task in enumerate(stop_tasks):
                task.join()
                print(f"Sender {i}: packets per MC {proc.monitor.heaps}, total packets {proc.monitor.total_heaps}")
                total_heaps += proc.monitor.total_heaps
            print(f"Total heaps sent: {total_heaps}")
            print("All mksend processes stopped")
            self._running = False