import os
from datetime import datetime
from mpikat.utils.mkrecv_stdout_parser import MkrecvSensors

from mpikat.utils.dada_tools.reader import DadaClient
from tempfile import NamedTemporaryFile

FBFUSE_HEADER_OUTPUT = """
HEADER        DADA
HDR_VERSION   1.0
HDR_SIZE      4096
DADA_VERSION  1.0
FILE_SIZE {buffer_size}
SAMPLE_CLOCK  {sample_clock}
IBV_VECTOR    -1
IBV_MAX_POLL  10
BUFFER_SIZE   33554432
PACKET_SIZE   1120
SAMPLE_CLOCK_START unset
NTHREADS      5
NHEAPS        64
NGROUPS_DATA  64
HEAP_NBYTES   {heap_size}
DADA_NSLOTS   3
SLOTS_SKIP    8
SLOT_SIZE     {buffer_size}

NINDICES    3
IDX1_ITEM   0
IDX1_STEP   {timestamp_step}
IDX1_MODULO {timestamp_modulus}

IDX2_ITEM   1
IDX2_LIST   {item_2_list}

IDX3_ITEM   2
IDX3_LIST   {item_3_list}
"""


class Mkreceiver(DadaClient):

    def __init__(self, key, nic_ip, mc_ip, port, desc, ibv=True):
        super().__init__(key)
        self.nic_ip = nic_ip
        self.mc_ip = mc_ip
        self.port = port
        self.key = key
        self.header = self.getHeaderFile(desc)
        self.ibv = ibv
        self.mkrecv_sensors = MkrecvSensors(str(key))
        self.line_cnt = 0
        self.cmd = "mkrecv_v4 --lst --quiet"
        self.cmd += f" --header {self.header}"
        self.cmd += f" --dada-key {self.key}"
        self.cmd += f" --sync-epoch {int(datetime.now().timestamp())}"
        self.cmd += f" --port {self.port}"
        if self.ibv:
            self.cmd += f" --ibv-if {self.nic_ip}"
        else:
            self.cmd += f" --udp-if {self.nic_ip}"
        self.cmd += " " + self.mc_ip

    def getHeaderFile(self, desc):
        with NamedTemporaryFile(delete=False, mode="w") as f:
            f.write(FBFUSE_HEADER_OUTPUT.format(**desc))
            name = f.name
        return name

