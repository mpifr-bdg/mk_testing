
from tempfile import NamedTemporaryFile

def generate_header(header: str, desc: dict) -> str:
    with NamedTemporaryFile(delete=False, mode="w") as f:
        f.write(header.format(**desc))
        return f.name

class FBFUSE_INPUT:
    HEADER = """
# DADA parameters
HEADER       DADA
HDR_VERSION  1.0
HDR_SIZE     4096
DADA_VERSION 1.0
UTC_START unset

PACKET_SIZE  1120
BUFFER_SIZE  8388080

RATE {data_rate}
NTHREADS {cores}
SAMPLE_CLOCK {sample_clock}

HEAP_SIZE {heap_size}
HEAP_ID_START {mksend_id}
HEAP_ID_OFFSET 0
HEAP_ID_STEP {n_sender}
HEAP_GROUP 16
NITEMS 7

ITEM1_ID     5632 # Timestamp index
ITEM1_STEP   {timestamp_step}
ITEM1_INDEX  1
ITEM2_ID     5633 # Antenna index
ITEM2_LIST   {item_2_list}
ITEM2_INDEX  3
ITEM3_ID     5634 # Channel index
ITEM3_LIST   {item_3_list}
ITEM3_INDEX  2
ITEM4_ID     5635 # No-op Item
ITEM4_LIST   1
ITEM5_ID     5636 # No-op Item
ITEM5_LIST   1
ITEM6_ID     5637 # No-op Item
ITEM6_LIST   1
ITEM7_ID     5638 # Payload
"""

    def __init__(self, desc: dict, n_sender: int):
        self.n_sender = n_sender
        self.desc = desc

    def get_header(self, send_id: int) -> str:

        conf = dict(self.desc)
        conf["mksend_id"] = send_id
        conf["n_sender"] = self.n_sender
        conf["data_rate"] = conf["data_rate"] / self.n_sender * 8
        conf["sample_clock"] = conf["sample_clock"] / self.n_sender
        conf["item_3_list"] = f"0:{conf['channels']}:1"
        if self.n_sender == conf['stations']:
            conf["item_2_list"] = f"{send_id},{send_id}"
        else:
            conf["item_2_list"] = f"{send_id}:{conf['stations']}:{self.n_sender}"
        return generate_header(self.HEADER, conf)






